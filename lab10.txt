
(define ADD(lambda (a b)         
		(Succ a (Succ b 0)) ))
		 
(define SUB(lambda (a b)         
		(Pred b a) ))
		
		(define TRUE(lambda (a b)         
		 a ))
		 
(define FALSE(lambda (a b)         
		 b ))
		
(define AND(lambda (M N)         
		(N (M "TRUE" "FALSE") "FALSE")))
		 
(define Or(lambda (M N)         
		 (N "TRUE" (M "TRUE" "FALSE"))))
		 
(define Not(lambda (M)         
		 (M "FALSE" "TRUE")))
		 
(define LEQ(lambda (a b)       
		(ISZERO (SUB a b)) ))

(define GEQ(lambda (a b)       
		(ISZERO (SUB b a)) ))
		 
(define (Succ n z) // Succ is successor
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define (Pred n z)// Pred is predecessor
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))
	  
(define (ISZERO n)
	  (if (zero? n)
		"TRUE"
		"FALSE"
	  ))